<?php
require_once('Db.php');
$dbInit = new Db();
$db = $dbInit->getDb();

$cat = 'product_cat';
$stmt = $db->prepare('DELETE FROM wp_terms where term_id in (SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = ? AND id_ext IS NULL)');
$stmt->bind_param('s', $cat);
$stmt->execute();

unset($stmt);
$stmt = $db->prepare('DELETE FROM wp_term_taxonomy where taxonomy = ? AND id_ext IS NULL');
$stmt->bind_param('s', $cat);
$stmt->execute();

unset($stmt);
$termId = 10000;
$stmt = $db->prepare('INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id) VALUES (?, ?)');
$stmt->bind_param('ii', $termId, $termId);
$stmt->execute();

unset($stmt);
$productId = 100000;
$stmt = $db->prepare('INSERT INTO wp_wc_product_meta_lookup (product_id) VALUES (?)');
$stmt->bind_param('i', $productId);
$stmt->execute();



//a:1:{s:7:"default";O:15:"THWEPOF_Section":19:{s:2:"id";s:7:"default";s:4:"name";s:7:"default";s:8:"position";s:29:"woo_before_add_to_cart_button";s:5:"order";s:0:"";s:8:"cssclass";s:0:"";s:15:"title_cell_with";s:0:"";s:15:"field_cell_with";s:0:"";s:10:"show_title";i:0;s:5:"title";s:7:"Default";s:10:"title_type";s:0:"";s:11:"title_color";s:0:"";s:11:"title_class";s:0:"";s:12:"cssclass_str";s:0:"";s:15:"title_class_str";s:0:"";s:12:"rules_action";s:0:"";s:22:"conditional_rules_json";s:0:"";s:17:"conditional_rules";a:0:{}s:14:"condition_sets";a:0:{}s:6:"fields";a:1:{s:4:"imei";O:29:"WEPOF_Product_Field_InputText":28:{s:5:"order";s:1:"0";s:4:"type";s:9:"inputtext";s:2:"id";s:4:"imei";s:4:"name";s:4:"imei";s:5:"value";s:1:"0";s:11:"placeholder";s:0:"";s:7:"options";a:0:{}s:9:"validator";s:6:"number";s:8:"cssclass";s:0:"";s:12:"cssclass_str";s:0:"";s:5:"title";s:11:"Device IMEI";s:11:"title_class";s:0:"";s:15:"title_class_str";s:0:"";s:14:"title_position";s:4:"left";s:8:"required";s:3:"yes";s:7:"enabled";s:1:"1";s:8:"readonly";i:0;s:8:"position";s:29:"woo_before_add_to_cart_button";s:22:"conditional_rules_json";s:127:"%5B%5B%5B%5B%7B%22subject%22%3A%22product%22%2C%22comparison%22%3A%22equals%22%2C%22cvalue%22%3A%5B%222807%22%5D%7D%5D%5D%5D%5D";s:17:"conditional_rules";a:1:{i:0;O:24:"WEPOF_Condition_Rule_Set":2:{s:5:"logic";s:3:"and";s:15:"condition_rules";a:1:{i:0;O:20:"WEPOF_Condition_Rule":2:{s:5:"logic";s:2:"or";s:14:"condition_sets";a:1:{i:0;O:19:"WEPOF_Condition_Set":2:{s:5:"logic";s:3:"and";s:10:"conditions";a:1:{i:0;O:15:"WEPOF_Condition":3:{s:7:"subject";s:7:"product";s:10:"comparison";s:6:"equals";s:5:"value";a:1:{i:0;s:4:"2807";}}}}}}}}}s:8:"name_old";s:4:"imei";s:12:"position_old";s:0:"";s:11:"input_class";s:0:"";s:9:"minlength";s:0:"";s:9:"maxlength";s:0:"";s:4:"cols";s:0:"";s:4:"rows";s:0:"";s:7:"checked";i:0;}}}}

//a:1:{s:7:"default";O:15:"THWEPOF_Section":19:{s:2:"id";s:7:"default";s:4:"name";s:7:"default";s:8:"position";s:29:"woo_before_add_to_cart_button";s:5:"order";s:0:"";s:8:"cssclass";s:0:"";s:15:"title_cell_with";s:0:"";s:15:"field_cell_with";s:0:"";s:10:"show_title";i:0;s:5:"title";s:7:"Default";s:10:"title_type";s:0:"";s:11:"title_color";s:0:"";s:11:"title_class";s:0:"";s:12:"cssclass_str";s:0:"";s:15:"title_class_str";s:0:"";s:12:"rules_action";s:0:"";s:22:"conditional_rules_json";s:0:"";s:17:"conditional_rules";a:0:{}s:14:"condition_sets";a:0:{}s:6:"fields";a:1:{s:4:"imei";O:29:"WEPOF_Product_Field_InputText":28:{s:5:"order";s:1:"0";s:4:"type";s:9:"inputtext";s:2:"id";s:4:"imei";s:4:"name";s:4:"imei";s:5:"value";s:1:"0";s:11:"placeholder";s:0:"";s:7:"options";a:0:{}s:9:"validator";s:6:"number";s:8:"cssclass";s:0:"";s:12:"cssclass_str";s:0:"";s:5:"title";s:11:"Device IMEI";s:11:"title_class";s:0:"";s:15:"title_class_str";s:0:"";s:14:"title_position";s:4:"left";s:8:"required";s:3:"yes";s:7:"enabled";s:1:"1";s:8:"readonly";i:0;s:8:"position";s:29:"woo_before_add_to_cart_button";s:22:"conditional_rules_json";s:130:"%5B%5B%5B%5B%7B%22subject%22%3A%22product_cat%22%2C%22comparison%22%3A%22equals%22%2C%22cvalue%22%3A%5B%22109%22%5D%7D%5D%5D%5D%5D";s:17:"conditional_rules";a:1:{i:0;O:24:"WEPOF_Condition_Rule_Set":2:{s:5:"logic";s:3:"and";s:15:"condition_rules";a:1:{i:0;O:20:"WEPOF_Condition_Rule":2:{s:5:"logic";s:2:"or";s:14:"condition_sets";a:1:{i:0;O:19:"WEPOF_Condition_Set":2:{s:5:"logic";s:3:"and";s:10:"conditions";a:1:{i:0;O:15:"WEPOF_Condition":3:{s:7:"subject";s:11:"product_cat";s:10:"comparison";s:6:"equals";s:5:"value";a:1:{i:0;s:3:"109";}}}}}}}}}s:8:"name_old";s:4:"imei";s:12:"position_old";s:0:"";s:11:"input_class";s:0:"";s:9:"minlength";s:0:"";s:9:"maxlength";s:0:"";s:4:"cols";s:0:"";s:4:"rows";s:0:"";s:7:"checked";i:0;}}}}