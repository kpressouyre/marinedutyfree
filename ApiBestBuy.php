<?php

/** 
* Class ApiBestBuy
*
* ApiBestBuy is a class who Interact with Best Buy Api
* we use only GET method from their api
* 
* @author Kevin Pressouyre <kevin.pressouyre@nixa.ca>
*/
class ApiBestBuy{

    private $ch;
    private $url;
    private $code;
    private $content;
    private $page;

    public function __construct(){
        $this->page = 1;
    }

    private function get(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        curl_setopt($ch, CURLOPT_AUTOREFERER, true); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $array = json_decode(curl_exec($ch));

        if(isset($array->errorCode)){
            $this->content['errorMessage'] = $array->errorMessage;
        }else{
            $this->content = $array;
        }
    }

    public function getProductsFromCategory($idCategory){
        unset($this->content);
        $this->url = "http://www.bestbuy.ca/api/v2/JSON/search?categoryid=".$idCategory."&pagesize=100&currentregion=QC&ignoreehfdisplayrestrictions=true&page=".$this->page;
        $json = $this->get();

        return $this->content;
    }

    public function getAllProducts(){
        unset($this->content);
        $this->url = "http://www.bestbuy.ca/api/v2/JSON/search?categoryid=departments&currentregion=QC&ignoreehfdisplayrestrictions=true&pagesize=100&page=".$this->page;
        $json = $this->get();

        return $this->content;
    }

    public function getParentCategories(){
        $this->url = "https://www.bestbuy.ca/api/v2/JSON/category/departments";
        $json = $this->get();

        return $this->content;
    }

    public function getSubCategories($subCategories){
        $this->url = "https://www.bestbuy.ca/api/v2/JSON/category/".$subCategories;
        $json = $this->get();

        return $this->content;
    }

    public function getProductInfo($idProduct){
        $this->url = "https://www.bestbuy.ca/api/v2/JSON/product/".$idProduct."?include=all";
        $json = $this->get();

        return $this->content;
    }

    public function nextPage(){
        $this->page++;
    }

    public function resetPage(){
        $this->page = 1;
    }

    public function getPage(){
        return $this->page;
    }
}