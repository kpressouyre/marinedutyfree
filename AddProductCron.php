<?php
require_once('ApiBestBuy.php');
require_once('Category.php');
require_once('Product.php');

$apiBestBuy = new ApiBestBuy();
$category = new Category();
$product = new Product();
$hasSubCat = $categSend = [];


$product->getAllSeo();
$skus = $product->getAllSkuIds();
$product->setHaveAllProducts(false);
$i = 1;
while(!$product->getHaveAllProducts()){
    $bestBuyProducts = $apiBestBuy->getAllProducts();
    $product->setProducts($bestBuyProducts);
    $product->addProduct();

    foreach($bestBuyProducts->products as $bestBuyProduct){
        if(in_array($bestBuyProduct->sku, $skus)){
            exit();
        }
    }

    $apiBestBuy->nextPage();
}
