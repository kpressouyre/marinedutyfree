<?php

/** 
* Class ApiCurrentChange
*
* ApiCurrentChange is a class who Interact with exchange rate
* we use only GET method from their api
* 
* @author Kevin Pressouyre <kevin.pressouyre@nixa.ca>
*/
class ApiCurrentChange{
    private $url;
    private function get(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        curl_setopt($ch, CURLOPT_AUTOREFERER, true); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        return json_decode(curl_exec($ch));
    }

    public function getCadToUsd(){
        $this->url = 'https://api.exchangeratesapi.io/latest?base=CAD';
        $json = $this->get();

        return $json->rates->USD;
    }
}