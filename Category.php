<?php
require_once('Db.php');

/** 
* Class Category
*
* Category add and update category to the database
* 
* @author Kevin Pressouyre <kevin.pressouyre@nixa.ca>
*/
class Category extends Db{
    private $categories;
    private $subCategories;
    private $maxId;
    private $categoriesInDb = [];
    private $subCategoriesInDb = [];
    private $parentId;
    private $parentsIDFromIdExt = [];
    private $allParentIdFromSubCategoryId = [];

    public function __construct(){
        $this->getDb();
    }

    public function setCategories($categories){
        $this->categories = $categories;
    }

    public function setSubCategories($subCategories){
        $this->subCategories = $subCategories;
    }

    /**
     * On récupère dans un premier temps les catégories déjà enregistrées dans la bdd
     * On retire de l'array ($this->categories) les catégories présentent
     */
    public function addCategories(){
        $this->query_getCategoriesTermId();
        $this->categories = is_array($this->categories) ? $this->categories : [$this->categories];
        $maxId = $this->db->query("SELECT MAX(term_taxonomy_id) FROM wp_term_taxonomy");
        $this->maxId = (int)$maxId->fetch_row()[0];
        
        foreach($this->categories as $key => $category){
            if(in_array($category->seoText, $this->categoriesInDb))
                unset($this->categories[$key]);
        }

        $this->query_addCategory_wp_term_taxonomy();
        $this->query_addCategory_wp_terms();
    }

    private function query_addCategory_wp_term_taxonomy(){   
        $stmt = $this->db->prepare("INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, id_ext) VALUES (?, ?, ?, ?, ?)");
        $maxId = $this->maxId;
        $cate = 'product_cat';
        $description = '';
        foreach($this->categories as $category){
            $maxId++;
            $stmt->bind_param('iisss', $maxId, $maxId, $cate, $description, $category->id);
            $stmt->execute();
        }
    }

    private function query_addCategory_wp_terms(){
        $maxId = $this->maxId;
        $stmt = $this->db->prepare("INSERT INTO wp_terms (term_id, name, slug) VALUES (?, ?, ?)");
        foreach($this->categories as $category){
            $maxId++;
            $stmt->bind_param('iss', $maxId, $category->name, $category->seoText);
            $stmt->execute();
        }
    }

    private function query_getCategoriesTermId(){
        $ids = $this->db->query("SELECT a.term_id, b.slug FROM wp_term_taxonomy a JOIN wp_terms b on (a.term_id = b.term_id) where taxonomy = 'product_cat'");
        while($row = $ids->fetch_assoc()){
            $this->categoriesInDb += [$row['term_id'] => $row['slug']];
        }
    }

    public function updateCategories(){
        $this->query_getCategoriesTermId();
        $this->categories = is_array($this->categories) ? $this->categories : [$this->categories];

        foreach($this->categories as $key => $category){
            if(in_array($category->seoText, $this->categoriesInDb)){
                $stmt = $this->db->prepare("SELECT term_id FROM wp_terms WHERE slug = ?");
                $stmt->bind_param('s', $category->seoText);
                $stmt->execute();
                $result = $stmt->get_result()->fetch_assoc();

                if(NULL != $result){
                    $stmt_wp_term_taxonomy = $this->db->prepare("UPDATE wp_term_taxonomy SET id_ext = ? WHERE term_id = ?");
                    $stmt_wp_term_taxonomy->bind_param('si', $category->id, $result['term_id']);
                    $stmt_wp_term_taxonomy->execute();

                    $stmt_wp_terms = $this->db->prepare("UPDATE wp_terms SET name = ? WHERE term_id = ?");
                    $stmt_wp_terms->bind_param('si', $category->name, $result['term_id']);
                    $stmt_wp_terms->execute();
                }
            }
        }
    }

    public function updateSubCategories(){
        $this->query_getSubCategoriesTermId();
        $this->subCategories = is_array($this->subCategories) ? $this->subCategories : [$this->subCategories];
        foreach($this->subCategories as $key => $category){
            if(in_array($category->seoText, $this->subCategoriesInDb)){
                $stmt = $this->db->prepare("SELECT term_id FROM wp_terms WHERE slug = ?");
                $stmt->bind_param('s', $category->seoText);
                $stmt->execute();
                $result = $stmt->get_result()->fetch_assoc();
                if(NULL != $result){
                    $stmt_wp_term_taxonomy = $this->db->prepare("UPDATE wp_term_taxonomy SET id_ext = ? WHERE term_id = ?");
                    $stmt_wp_term_taxonomy->bind_param('si', $category->id, $result['term_id']);
                    $stmt_wp_term_taxonomy->execute();

                    $stmt_wp_terms = $this->db->prepare("UPDATE wp_terms SET name = ? WHERE term_id = ?");
                    $stmt_wp_terms->bind_param('si', $category->name, $result['term_id']);
                    $stmt_wp_terms->execute();
                }
            }
        }
    }


    public function addSubCategories(){
        $this->query_getSubCategoriesTermId();
        $this->getParentsFromIdExt();
        $maxId = $this->db->query("SELECT MAX(term_taxonomy_id) FROM wp_term_taxonomy");
        $this->maxId = (int)$maxId->fetch_row()[0];

        foreach($this->subCategories as $key => $subCategory){
            if(in_array($subCategory->seoText, $this->subCategoriesInDb))
                unset($this->subCategories[$key]);
        }

        $this->query_subCategory_wp_term_taxonomy();
        $this->query_subCategory_wp_terms();
    }

    private function query_subCategory_wp_term_taxonomy(){   
        $stmt = $this->db->prepare("INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count, id_ext) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $maxId = $this->maxId;
        $cate = 'product_cat';
        $description = '';
        $parent = array_key_exists($this->parentId, $this->parentsIDFromIdExt) ? $this->parentsIDFromIdExt[$this->parentId] : 0;

        foreach($this->subCategories as $subCategory){
            $maxId++;
            $stmt->bind_param('iissiii', $maxId, $maxId, $cate, $description, $parent, $subCategory->productCount, $subCategory->id);
            $stmt->execute();
        }
    }

    private function query_subCategory_wp_terms(){
        $maxId = $this->maxId;
        $stmt = $this->db->prepare("INSERT INTO wp_terms (term_id, name, slug) VALUES (?, ?, ?)");
        foreach($this->subCategories as $subCategory){
            if($subCategory->id == 28383)
                $subCategory->seoText = 'ereader-accessories';
            $maxId++;
            $stmt->bind_param('iss', $maxId, $subCategory->name, $subCategory->seoText);
            $stmt->execute();
        }
    }

    private function query_getSubCategoriesTermId(){
        $ids = $this->db->query("SELECT a.term_id, b.slug FROM wp_term_taxonomy a JOIN wp_terms b on (a.term_id = b.term_id) where taxonomy = 'product_cat'");
        while($row = $ids->fetch_assoc()){
            $this->subCategoriesInDb += [$row['term_id'] => $row['slug']];
        }
    }

    public function getAllCategoriesByName(){
        $categories = [];
        $ids = $this->db->query("SELECT a.term_id, b.name FROM wp_term_taxonomy a JOIN wp_terms b on (a.term_id = b.term_id) where taxonomy = 'product_cat' OR taxonomy = 'category'");
        while($row = $ids->fetch_assoc()){
            $categories += [$row['name'] => $row['term_id']];
        }
        return $categories;
    }

    public function getParentsFromIdExt(){
        $query = $this->db->query("SELECT a.term_taxonomy_id, b.slug FROM wp_term_taxonomy a 
        JOIN wp_terms b on (a.term_id = b.term_id) WHERE a.taxonomy = 'product_cat'");
        while($row = $query->fetch_assoc())
            $this->parentsIDFromIdExt[$row['slug']] = $row['term_taxonomy_id'];
    }

    public function getAllParentsFromSubCategoryId($subCategoryId){
        $parent = $subCategoryId;
        if(!isset($this->allParentIdFromSubCategoryId[$subCategoryId])){
            while($parent != 0){
                $query = $this->db->query("SELECT parent FROM wp_term_taxonomy WHERE term_taxonomy_id = ".$parent);
                $this->allParentIdFromSubCategoryId[$subCategoryId][] = $parent = $query->fetch_assoc()['parent'];
                unset($query);
            }
        }

        return $this->allParentIdFromSubCategoryId;
    }

    public function setParentId($parentId){
        $this->parentId = $parentId;
    }
}