<?php
	class UCLAPI {
		private $clientId = "5d27ff5e5638647178225d39";
		private $clientSecret = "5d27ff5e5638647178225d3a";
		private $userCode = "Northstar_API";
		private $langType = "en-US";
		private $password = "c136c142b9bff1a9e4b6a4911195ef4a";  
		private $partnerCode = "Northstar";
		private $mvnoCode = "Northstar";
		private $accessToken = null;
		private $loginCustomerId = null;

		private function getTimeToMicroseconds() {
			$t = microtime(true);
			$micro = sprintf("%06d", ($t - floor($t)) * 1000000);
			return date('YmdHis' . $micro);
		}

		private function formData($input = null) {
			$arr = array();
			$arr["streamNo"] = $this->partnerCode . $this->getTimeToMicroseconds();
			$arr["langType"] = $this->langType;
			$arr["partnerCode"] = $this->partnerCode;
			if ($this->accessToken != null && $this->loginCustomerId != null) {
				$arr["accessToken"] = $this->accessToken;
				$arr["loginCustomerId"] = $this->loginCustomerId;
			} else {
				$arr["clientId"] = $this->clientId;
				$arr["clientSecret"] = $this->clientSecret;
				$arr["userCode"] = $this->userCode;
				$arr["password"] = $this->password;
				$arr["mvnoCode"] = $this->mvnoCode;
			}
			if ($input != null) {
				$arr = array_merge($arr, $input);
			}
			return $arr;
		}

		private function callAPI($url, $method, $data) {

			$curl = curl_init();
			switch ($method) {
				case "POST":
					curl_setopt($curl, CURLOPT_POST, 1);
					if ($data) curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
					break;
				case "PUT":
					curl_setopt($curl, CURLOPT_PUT, 1);
					break;
				default:
					if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
			}

			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			printf("\n----Call----%s\n%s\n", $url, $data);
			$result = curl_exec($curl); 
			printf("\n----Done----\n%s\n", $result);
			curl_close($curl);
			return json_decode($result);
		}	
		
		public function callLogin() {
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/noauth/GrpUserLogin", "POST", json_encode($this->formData()));
			$this->accessToken = $result->data->accessToken;
			$this->loginCustomerId = $result->data->userId;
		}

		public function callLogout() {
			$this->callAPI("https://saas.ucloudlink.com/bss/grp/user/GrpUserLogout?access_token=$this->accessToken", "POST", json_encode($this->formData()));
		}

		function __construct() {
			$this->callLogin();
		}

		function __destruct() {
			$this->callLogout();
		}

		
		public function createSubUser($userCode, $userPwd, $imei=null, $tmlPwd=null, $registerType = "EMAIL") {
			$childUserVo = array();
			$childUserVo["userCode"] = $userCode;
			$childUserVo["registerType"] = $registerType; // EMAIL or PHONE
			$childUserVo["userPwd"] = $userPwd;
			if ($imei != null and $tmlPwd != null) {
				$childUserVo["imei"] = $imei;
				$childUserVo["tmlPwd"] = $tmlPwd;
			}
			$childUserVoList = array();
			$childUserVoList[] = $childUserVo;
			$input = array();
			$input["childUserVoList"] = $childUserVoList;
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/user/BatchCreateGroupChild?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}

		public function querySubUser($userCode) {
			$input = array();
			$input["userCode"] = $userCode;
			$input["currentPage"] = "1";
			$input["perPageCount"] = "100";
			$input["deleteFlag"] = "1";
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/user/QuerySubUserListInfo?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}



		public function queryOfferList($categoryCode = "LLTC", $channelType = "WEB") {
			//$categoryCode:   LLTC: Data bundle; ZLTC: Daily bundle; BYTCï¼šMonthly bundle;  
			//$channelType:    DISC: Preferential goods ; PKAG: Flow package; COMM: common goods; ALL: The above mentioned; 
			$input = array();
			$input["categoryCode"] = $categoryCode;
			$input["channelType"] = $channelType;
			$input["currentPage"] = "1";
			$input["perPageCount"] = "100";
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/goods/QueryGrpOfferList?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}

		public function queryOrder($userCode, $goodsType = "ALL", $flag = "0") {
			$input = array();
			$input["userCode"] = $userCode;
			$input["flag"] = $flag;
			$input["goodsType"] = $goodsType;
			$input["perPageCount"] = 1;
			$input["currentPage"] = 1;
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/goods/QueryUserOfferList?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}

		public function cancelOrder($orderId) {
			$input = array();
			$input["orderId"] = $orderId;
			$result = $this->callAPI("https://saas.ucloudlink.com//bss/grp/order/CancelOrder?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}

		public function deleteSubUser($userCode) {
			$input = array();
			$input["operateType"] = "0";
			$subUserList = array();
			$subUserList[] = $userCode;
			$input["userCodeList"] = $subUserList;
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/user/DeleteSubUser?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}

		public function createOrder($userCode, $goodsId, $orderMark = "API Test", $channelType = "GRP", $orderType = "BUYPKG", $payMethod = "ACCOUNT_AMOUNT", $currencyType = "USD") {
			$input = array();
			$input["userCode"] = $userCode;
			$input["channelType"] = $channelType;
			$input["orderType"] = $orderType;
			$input["payMethod"] = $payMethod;
			$input["currencyType"] = $currencyType;
			$input["orderMark"] = $orderMark;
			$goods = array();
			$goods["goodsId"] = $goodsId;
			$goods["quanity"] = 1;
			$goodsList = array();
			$goodsList[] = $goods;
			$input["goodsList"] = $goodsList;
			$result = $this->callAPI("https://saas.ucloudlink.com/bss/grp/order/GrpCreateOrder?access_token=$this->accessToken", "POST", json_encode($this->formData($input)));
			return $result;
		}
	}

	/**********************************/
	$u = new UCLAPI();
	//$u->createSubUser("864652035993966@lily.com", "123456", "864652035993966", "00969966");
	//$u->querySubUser("yk02@test.com");
	//$u->queryOfferList("LLTC", "SAAS");
	//$u->createOrder("864652038862804@lily.com", "5d4164ba16d3db5b35448b95");
	//$u->queryOrder("yk02@test.com");
	//$u->cancelOrder("5d2cfe5016d3db76a2da9157");
	//$u->deleteSubUser("yk02@test.com");
	/********************************/
	/********************************/
	/********************************/
	/********************************/
	// Step 1:
	//$u->createSubUser("861434040053448@pierre.ca", "123456", "861434040053448", "03344443");
	/********************************/
	// Step 2:
	//$u = new UCLAPI();
	//$u->querySubUser("861434040053448@pierre.ca");
	//$result = $u->createOrder("861434040053448@pierre.ca", "5d28406416d3db76a2648a79");
	//if ($result->resultCode == "00000000") {
		//echo "successful";
	//} else {
	//	echo "failed";
	//}
?>