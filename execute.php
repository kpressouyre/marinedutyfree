<?php
require_once('ApiBestBuy.php');
require_once('Category.php');
require_once('Product.php');

$apiBestBuy = new ApiBestBuy();
$category = new Category();
$product = new Product();
$hasSubCat = $categSend = [];

$parentCategories = $apiBestBuy->getParentCategories();
$category->setCategories($parentCategories->subCategories);
$category->updateCategories();
$category->addCategories();
foreach($parentCategories->subCategories as $cat){
    if($cat->hasSubcategories){
        $hasSubCat[] = $cat->id;
    }
}

$categSend = $hasSubCat;

while(!empty($hasSubCat)){
    $subCategories = [];
    $subCategory = array_shift($hasSubCat);
    $tmp = $apiBestBuy->getSubCategories($subCategory);
    $category->setParentId($tmp->seoText);
    foreach($tmp->subCategories as $subCat){
        $subCategories[] = $subCat;
        if($subCat->hasSubcategories && !in_array($subCat->id, $categSend)){
            $hasSubCat[] = $subCat->id;
            $categSend[] = $subCat->id;
        }
    }
    $category->setSubCategories($subCategories);
    $category->updateSubCategories();
    $category->addSubCategories();
}


$product->getAllSeo();
$product->getAllSkuIds();
$product->setHaveAllProducts(false);
while(!$product->getHaveAllProducts()){
    $product->setProducts($apiBestBuy->getAllProducts());
    $product->addProduct();

    if($product->getProduct()->totalPages == $apiBestBuy->getPage())
        $product->setHaveAllProducts(true);

    $apiBestBuy->nextPage();
}
