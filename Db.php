<?php

/** 
* Class Db
*
* Init the database conenction
* 
* @author Kevin Pressouyre <kevin.pressouyre@nixa.ca>
*/
class Db{
    protected $db;

    public function __construct(){
    }

    public function getDb(){
        $this->db = new mysqli("127.0.0.1", "root", "root", "northstarship_mdf", "8889");
        if ($this->db->connect_errno)
            $this->db = false;

        return $this->db;
    }
}