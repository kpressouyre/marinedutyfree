<?php
require_once('Db.php');

$db = new Db();
$db = $db->getDb();
const FIRST_DATA_PRICE = 4;
const LAST_DATA_PRICE = 12;
const COUNTRY = 1;
const REPLACE_CARACTER = '/[,& ()]/';
const REPLACE_DOUBLON = '/-{2,}/';

$postMeta = ['_wp_page_template' => 'default',
                        '_elementor_controls_usage' => 'a:0:{}',
                        '_deal_sales_count' => 0,
                        '_deal_quantity' => '',
                        '_wc_review_count' => 0,
                        '_wc_rating_count' => 0,
                        '_wc_average_rating' => 0,
                        '_total_sales' => 0,
                        '_tax_status' => 'none',
                        '_manage_stock' => 'no',
                        '_backorders' => 'no',
                        '_sold_individually' => 'no',
                        '_virtual' => 'no',
                        '_downloadable' => 'no',
                        '_download_limit' => -1,
                        '_download_expiry' => 'no',
                        '_stock' => 'no',
                        '_stock_status' => 'no',
                        '_product_attributes' => 'no',
                        '_product_version' => 'no',
                        '_thumbnail_id' => '',
                        '_product_image_gallery' => ''
                    ];


if (($handle = fopen("telecommunicationProduct.csv", "r")) !== FALSE) {
    $infoDataDay = [];
    $data = fgetcsv($handle, 1000, ",");
    for($i = FIRST_DATA_PRICE; $i <= LAST_DATA_PRICE; $i++){
        $infoDataDay[$i] = str_replace(' ', '', $data[$i]);
    }

    $infoDataCaps = [];
    $data = fgetcsv($handle, 1000, ",");
    for($i = FIRST_DATA_PRICE; $i <= LAST_DATA_PRICE; $i++){
        $infoDataCaps[$i] = str_replace(' ', '', $data[$i]);
    }

    $maxId = $db->query("SELECT MAX(product_id) FROM wp_wc_product_meta_lookup");
    $lastId = (int)$maxId->fetch_row()[0];
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        for($i = FIRST_DATA_PRICE; $i <= LAST_DATA_PRICE; $i++){
            $country = preg_replace(REPLACE_CARACTER, '-', $data[COUNTRY]);
            $sku = preg_replace(REPLACE_DOUBLON, '-', "telecommunication-".$country."-".$infoDataDay[$i]."-".$infoDataCaps[$i]);
            $price = (float)str_replace('$', '', $data[$i]);
            if(0 != $price){
                $lastId++;
                $stmt = $db->prepare("INSERT INTO wp_wc_product_meta_lookup (product_id, sku, min_price, max_price, stock_status) VALUES (?, ?, ?, ?, ?)");
                $stock = 'instock';
                $stmt->bind_param('iidds', $lastId, $sku, $price, $price, $stock);
                $stmt->execute();

                $date = date('Y-m-d H:i:s');
                $dateGmt = gmdate('Y-m-d H:i:s');
                $author = 1;
                $type = 'product';
                $postStatus = 'publish';
                $commentStatus = 'closed';
                $pingStatus = 'open';
                $nullString = '';
                $name = str_replace('-', ' ', $sku);
                $stmt = $db->prepare("INSERT INTO wp_posts (id, post_author, post_date, post_date_gmt, post_content, post_title, post_status, comment_status, ping_status, post_name, post_modified, post_modified_gmt, guid, post_type, post_excerpt, to_ping, pinged, post_content_filtered) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $guid = "//marinedutyfree.com/shop/".$sku;
                $stmt->bind_param('iissssssssssssssss', $lastId, $author, $date, $dateGmt, $name,
                        $name, $postStatus, $commentStatus, $pingStatus, $sku, $date, $dateGmt, $guid, $type, $nullString, $nullString, $nullString, $nullString);
                $stmt->execute();       

                $idTelecommunication = 109;
                $order = 1;
                $stmt = $db->prepare("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (?, ?, ?)");
                $stmt->bind_param('iii', $lastId, $idTelecommunication, $order);
                $stmt->execute();

                $stmt = $db->prepare("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES (?, ?, ?)");
                $postMeta['_sku'] = $sku;
                $postMeta['_price'] = $price;
                $postMeta['_regular_price'] = $price;
  
                foreach($postMeta as $metaKey => $metaValue){
                    $stmt->bind_param('iss', $lastId, $metaKey, $metaValue);
                    $stmt->execute();
                }
            }
        }
    }
    fclose($handle);
}