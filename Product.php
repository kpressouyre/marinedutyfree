<?php
require_once('Db.php');
require_once('Category.php');
require_once('ApiBestBuy.php');
require_once('ApiCurrentChange.php');

/** 
* Class Product
*
* Product add and update product to the database
* 
* @author Kevin Pressouyre <kevin.pressouyre@nixa.ca>
*/
class Product extends Db{
    private $products;
    private $maxId;
    private $idsSku = [];
    private $haveAllProducts;
    private $category;
    private $allCategories = [];
    private $allProducts = [];
    private $apiBestBuy;
    private $allSeo = [];
    private $CADToUSD;
    CONST TAXE_MARINE_DUTY_FREE = 15;

    private $postMeta1 = [];

    public function __construct(){
        $apiCurrentChange = new ApiCurrentChange();
        $this->CADToUSD = $apiCurrentChange->getCadToUsd();
        $this->getDb();
        $this->haveAllProducts = false;
        $this->category = new Category();
        $this->apiBestBuy = new ApiBestBuy();

        $this->postMeta1 = ['_wp_page_template' => 'default',
                        '_elementor_controls_usage' => 'a:0:{}',
                        '_deal_sales_count' => 0,
                        '_deal_quantity' => '',
                        '_wc_review_count' => 0,
                        '_wc_rating_count' => 0,
                        '_wc_average_rating' => 0,
                        '_total_sales' => 0,
                        '_sku' => '->sku',
                        '_price' => 'regularPrice',
                        '_regular_price' => 'regularPrice',
                        '_tax_status' => 'none',
                        '_manage_stock' => 'no',
                        '_backorders' => 'no',
                        '_sold_individually' => 'no',
                        '_virtual' => 'no',
                        '_downloadable' => 'no',
                        '_download_limit' => -1,
                        '_download_expiry' => 'no',
                        '_stock' => 'no',
                        '_stock_status' => 'no',
                        '_product_attributes' => 'no',
                        '_product_version' => 'no',
                        '_thumbnail_id' => '',
                        '_product_image_gallery' => ''
                    ];

        $this->postMeta2 = ['_wp_attached_file' => '->highResImage',
                    '_wp_attachment_metadata' => 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:18:"martfury-blog-grid";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:380;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"martfury-blog-masonry";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:370;s:6:"height";i:370;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:0;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"SKUIMAGETOREPLACE.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'
                ];
    }

    public function setProducts($products){
        unset($this->products);
        $this->products = $products;
    }

    /**
     * Ajoute le produit dans la bdd
     * Dans un premier temps on retire de notre attribut product
     * Les produits déjà présent dans la bdd
     * 
     * query_add_table => INSERT INTO table
     */
    public function addProduct(){
        if(empty($this->allCategories)){
            $this->allCategories = $this->category->getAllCategoriesByName();
        }

        $maxId = $this->db->query("SELECT MAX(product_id) FROM wp_wc_product_meta_lookup");
        $this->maxId = (int)$maxId->fetch_row()[0];

        foreach($this->products->products as $key => $product){
            if(in_array($product->sku, $this->idsSku) || !isset($product->isMarketplace) || $product->isMarketplace == true){
                unset($this->products->products[$key]);
            }else{
                $i = 1;
                $this->idsSku[] = $product->sku;
                $tmpSeoTxt = $product->seoText;
                while(in_array($tmpSeoTxt, $this->allSeo)){
                    $tmpSeoTxt = $product->seoText."-".$i;
                    $i++;
                }

                $product->seoText = $tmpSeoTxt;
                $this->allSeo[] = $product->seoText;
            }
        }

        
        $this->query_add_wp_wc_product_meta_lookup();
        $this->query_add_wp_posts_1();
        $this->query_add_wp_posts_2();
        $this->query_add_wp_pmxi_posts();
        $this->query_add_wp_pmxi_images();
        $this->query_add_category_wp_term_relationships();
        $this->query_add_wp_postmeta1();
        $this->query_add_wp_postmeta2();
    }

    private function query_add_wp_wc_product_meta_lookup(){
        $stmt = $this->db->prepare("INSERT INTO wp_wc_product_meta_lookup (sku, min_price, max_price, product_id, stock_status) VALUES (?, ?, ?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId++;
        $stock = 'instock';
        foreach($this->products->products as $product){
            $price = $this->convertCADToUSD(($product->regularPrice + $product->ehf));
            $stmt->bind_param('sddis', $product->sku, $price, $price, $maxId, $stock);
            $stmt->execute();
            $maxId += 2;
        }
    }

    private function query_add_wp_pmxi_posts(){
        $stmt = $this->db->prepare("INSERT INTO wp_pmxi_posts (post_id, import_id, unique_key) VALUES (?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId++;
        $importId = 1;
        foreach($this->products->products as $product){
            $stmt->bind_param('iis', $maxId, $importId, $product->name);
            $stmt->execute();
            $maxId += 2;
        }
    }

    private function query_add_wp_posts_1(){
        $stmt = $this->db->prepare("INSERT INTO wp_posts (id, post_author, post_date, post_date_gmt, post_content, post_title, post_status, comment_status, ping_status, post_name, post_modified, post_modified_gmt, guid, post_type, post_excerpt, to_ping, pinged, post_content_filtered) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId++;
        $date = date('Y-m-d H:i:s');
        $dateGmt = gmdate('Y-m-d H:i:s');
        $author = 1;
        $type = 'product';
        $postStatus = 'publish';
        $commentStatus = 'closed';
        $pingStatus = 'open';
        $nullString = '';

        foreach($this->products->products as $key => $product){
            $guid = "//marinedutyfree.com/shop/".$product->seoText;
            $stmt->bind_param('iissssssssssssssss', $maxId, $author, $date, $dateGmt, $product->shortDescription,
                    $product->name, $postStatus, $commentStatus, $pingStatus, $product->seoText, $date, $dateGmt, $guid, $type, $nullString, $nullString, $nullString, $nullString);
            $stmt->execute();
            $maxId += 2;
        }
    }

    private function query_add_wp_posts_2(){
        $stmt = $this->db->prepare("INSERT INTO wp_posts (id, post_author, post_date, post_date_gmt, post_title, post_status, comment_status, ping_status, post_name, post_modified, post_modified_gmt, post_parent, guid, post_type, post_mime_type, post_excerpt, to_ping, pinged, post_content_filtered, post_content) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId += 2;
        $date = date('Y-m-d H:i:s');
        $dateGmt = gmdate('Y-m-d H:i:s');
        $author = 1;
        $type = 'attachment';
        $postStatus = 'inherit';
        $commentStatus = 'open';
        $pingStatus = 'closed';
        $mime = 'image/jpeg';
        $nullString = '';

        foreach($this->products->products as $key => $product){
            $guid = "//marinedutyfree.com/shop/".$product->seoText;
            $productId = ($maxId - 1);
            $stmt->bind_param('iississsississssssss', $maxId, $author, $date, $dateGmt, $product->sku, $postStatus, $commentStatus,
                    $pingStatus, $product->sku, $date, $dateGmt, $productId, $product->highResImage, $type, $mime, $nullString, $nullString, $nullString, $nullString, $nullString);
            $stmt->execute();
            $maxId += 2;
        }
    }

    private function query_add_wp_pmxi_images(){
        $stmt = $this->db->prepare("INSERT INTO wp_pmxi_images (image_url, attachment_id, image_filename) VALUES (?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId += 2;

        foreach($this->products->products as $product){
            $imageFile = explode('/', $product->highResImage);
            $stmt->bind_param('sis', $product->highResImage, $maxId, end($imageFile));
            $stmt->execute();
            $maxId += 2;
        }
    }

    private function query_add_category_wp_term_relationships(){   
        $stmt = $this->db->prepare("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId++;

        foreach($this->products->products as $product){
            $subCategoryId = $this->allCategories[$product->categoryName];
            $allCategory = $this->category->getAllParentsFromSubCategoryId($subCategoryId)[$subCategoryId];
            if(is_array($allCategory)){
            $term_order = count($allCategory);
            foreach($allCategory as $category)
            {
                $stmt->bind_param('iii', $maxId, $category, $term_order);
                $stmt->execute();
                $term_order--;
            }
        }
            $maxId += 2;
        }
    }

    private function query_add_wp_postmeta1(){
        $stmt = $this->db->prepare("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES (?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId++;

        foreach($this->products->products as $product){
            foreach($this->postMeta1 as $metaKey => $metaValue){
                if(substr($metaValue, 0, 2) == '->'){
                    $attr = substr($metaValue, 2);
                    $metaValue = $product->$attr;
                }

                if($metaKey == '_thumbnail_id')
                    $metaValue = $maxId + 1;

                if($metaKey == '_price' || $metaKey == '_regular_price')
                    $metaValue = $this->convertCADToUSD(($product->regularPrice + $product->ehf));

                $stmt->bind_param('iss', $maxId, $metaKey, $metaValue);
                $stmt->execute();
            }
            $maxId += 2;
        }
    }

    private function query_add_wp_postmeta2(){
        $stmt = $this->db->prepare("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES (?, ?, ?)");
        $maxId = $this->maxId;    
        $maxId += 2;

        foreach($this->products->products as $product){
            foreach($this->postMeta2 as $metaKey => $metaValue){
                if(substr($metaValue, 0, 2) == '->'){
                    $attr = substr($metaValue, 2);
                    $metaValue = $product->$attr;
                }else{
                    $metaValue = str_replace('SKUIMAGETOREPLACE', $product->sku, $metaValue);
                }
                $stmt->bind_param('iss', $maxId, $metaKey, $metaValue);
                $stmt->execute();
            }
            $maxId += 2;
        }
    }

    /**
     * Récupère tous les sku déjà présent dans la bdd
     */
    public function getAllSkuIds(){
        $this->idsSku = [];
        $query = $this->db->query("SELECT sku FROM wp_wc_product_meta_lookup WHERE sku <> 0");
        while($row = $query->fetch_assoc())
            $this->idsSku[] = $row['sku'];

        return $this->idsSku;
    }

    /**
     * Récupère le seo de tous les produits déjà présent dans la bdd
     */
    public function getAllSeo(){
        $this->allSeo = [];
        $query = $this->db->query("SELECT post_name FROM wp_posts");
        while($row = $query->fetch_assoc())
            $this->allSeo[] = $row['post_name'];

        return $this->allSeo;
    }


    /**
     * Mis à jour des produits dans la bdd
     * Dans un premier temps on retire de notre attribut product
     * Les produits non présent dans la bdd
     * 
     * query_update_table => UPDATE table
     */
    public function updateProduct(){
        if(!isset($this->products->products) || count($this->products->products) < 100 )
            $this->haveAllProducts = true;

        if(empty($this->allCategories)){
            $this->allCategories = $this->category->getAllCategoriesByName();
        }
        $this->setAllProductsFromSku();

        foreach($this->products->products as $key => $product){
            if(!in_array($product->sku, $this->idsSku))
                unset($this->products->products[$key]);
        }

        $this->query_update_wp_wc_product_meta_lookup();
        $this->query_update_wp_posts_1();
        $this->query_update_wp_pmxi_posts();
        $this->query_update_wp_pmxi_images();
        $this->query_update_category_wp_term_relationships();
    }

    private function query_update_wp_wc_product_meta_lookup(){
        $stmt = $this->db->prepare("UPDATE wp_wc_product_meta_lookup set min_price = ?, max_price = ?, stock_status = ?
                WHERE product_id = ?");

        $stock = 'instock';
        foreach($this->products->products as $product){
            $substractPercentage = $this->getSubstractPercentage($this->allProducts[$product->sku]);
            $price = $this->convertCADToUSD(($product->regularPrice + $product->ehf), $substractPercentage);
            $stmt->bind_param('ddss', $price, $price, $stock, $this->allProducts[$product->sku]);
            $stmt->execute();
        }
    }

    private function query_update_post_meta(){
        $metaKeyPrice = '_price';
        $metaKeyRegularPrice = '_regular_price';
        $stmt = $this->db->prepare("UPDATE wp_postmeta SET meta_value = ? WHERE post_id = ? AND (meta_key = ? OR meta_key = ?)");
        
        foreach($this->products->products as $product){
            $substractPercentage = $this->getSubstractPercentage($this->allProducts[$product->sku]);
            $price = $this->convertCADToUSD(($product->regularPrice + $product->ehf), $substractPercentage);
            $stmt->bind_param('diss', $price, $this->allProducts[$product->sku], $metaKeyPrice, $metaKeyRegularPrice);
            $stmt->execute();
        }
    }

    private function query_update_wp_pmxi_posts(){
        $stmt = $this->db->prepare("UPDATE wp_pmxi_posts set unique_key = ? WHERE post_id = ?");
        $maxId = $this->maxId;    
        $maxId++;
        $importId = 1;
        foreach($this->products->products as $product){
            $stmt->bind_param('si', $product->name, $this->allProducts[$product->sku]);
            $stmt->execute();
        }
    }

    private function query_update_wp_posts_1(){
        $stmt = $this->db->prepare("UPDATE wp_posts SET post_content = ?, post_title = ?, comment_status = ?, ping_status = ?, post_name = ?, post_modified = ?, post_modified_gmt = ?, guid = ?, post_type = ?
                WHERE id = ?"); 
        $date = date('Y-m-d H:i:s');
        $dateGmt = gmdate('Y-m-d H:i:s');
        $type = 'product';
        $postStatus = 'publish';
        $commentStatus = 'closed';
        $pingStatus = 'open';
        foreach($this->products->products as $product){
            $guid = "//marinedutyfree.com/shop/".$product->seoText;
            $stmt->bind_param('sssssssssi', $product->shortDescription, $product->name, $commentStatus, 
                $pingStatus, $product->seoText, $date, $dateGmt, $guid, $type, $this->allProducts[$product->sku]);
            $stmt->execute();
        }
    }

    private function query_update_wp_pmxi_images(){
        $stmt = $this->db->prepare("UPDATE wp_pmxi_images set image_url = ?, image_filename = ? WHERE attachment_id = ?");

        foreach($this->products->products as $product){
            $attachmentId = $this->allProducts[$product->sku] + 1;
            $imageFile = explode('/', $product->highResImage);
            $stmt->bind_param('ssi', $product->highResImage, end($imageFile), $attachmentId);
            $stmt->execute();
        }
    }

    private function query_update_category_wp_term_relationships(){   
        $stmt = $this->db->prepare("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (?, ?, ?)");

        foreach($this->products->products as $product){
            $stmtExist = $this->db->prepare("SELECT a.object_id FROM wp_term_relationships a join wp_terms b on (a.term_taxonomy_id = b.term_id) WHERE a.object_id = ? AND b.name = ?");
            $stmtExist->bind_param('is', $this->allProducts[$product->sku], $product->categoryName);
            $exist = $stmtExist->get_result();
            if($exist->num_rows === 0){
                $stmtDelete = $this->db->prepare("DELETE FROM wp_term_relationships WHERE object_id = ?");
                $stmtDelete->bind_param('i', $this->allProducts[$product->sku]);
                $stmtDelete->execute();
                $subCategoryId = $this->allCategories[$product->categoryName];
                $allCategory = $this->category->getAllParentsFromSubCategoryId($subCategoryId)[$subCategoryId];
                $term_order = count($allCategory);
                foreach($allCategory as $category)
                {
                    $stmt->bind_param('iii', $maxId, $category, $term_order);
                    $stmt->execute();
                    $term_order--;
                }
            }
        }
    }

    public function getHaveAllProducts(){
        return $this->haveAllProducts;
    }

    public function setHaveAllProducts($haveAllProducts){
        return $this->haveAllProducts = $haveAllProducts;
    }

    /**
     * Retourne un array de tous les produits 
     * Avec comme clé le sku du produit et en valeur l'id Wordpress du produit
     */
    public function setAllProducts(){
        $query = $this->db->query("SELECT product_id, sku FROM wp_wc_product_meta_lookup");
        while($row = $query->fetch_assoc())
            $this->allProducts[$row['sku']] = $row['product_id'];
    }

    /**
     * Retourne un array des produits renseigné dans l'attribut produt
     * Avec comme clé le sku du produit et en valeur l'id Wordpress du produit
     */
    public function setAllProductsFromSku(){
        $sku = [];
        foreach($this->products->products as $product){
            $sku[] = $product->sku;
        }
        $query = $this->db->query("SELECT product_id, sku FROM wp_wc_product_meta_lookup WHERE sku IN ('".implode("','", $sku)."')");
        while($row = $query->fetch_assoc())
            $this->allProducts[$row['sku']] = $row['product_id'];
    }

    /**
     * Mets à jour le produit dont l'id Wordpress à été passé en paramètre
     * Ajouts des images, Mise à jour du prix, Mise à jour des informations
     */
    public function updateProductInfo($idProduct){
        $modifyStock = $modifySpec = $modifyPrice = false;
        $stmt = $this->db->prepare("SELECT sku, stock_status FROM wp_wc_product_meta_lookup WHERE product_id = ?");
        $stmt->bind_param('i', $idProduct);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_assoc();
        $sku = $result['sku'];

        if($sku == 0){
            $metaKey = '_sku';
            $stmt = $this->db->prepare("SELECT meta_value FROM wp_postmeta WHERE post_id = ? AND meta_key = ?");
            $stmt->bind_param('is', $idProduct, $metaKey);
            $stmt->execute();
            $resultSku = $stmt->get_result()->fetch_assoc();
            $sku = $resultSku['meta_value'];

            if($sku == NULL)
                return;
        }

        $product = $this->apiBestBuy->getProductInfo($sku);
        if($product == null)
            return;

        $substractPercentage = $this->getSubstractPercentage($idProduct);
        $actualPrice = $this->getActualPrice($idProduct);
        $newPrice = $this->convertCADToUSD(($product->regularPrice + $product->ehf), $substractPercentage);
        if($newPrice != $actualPrice){
            $modifyPrice = true;
            $stmt = $this->db->prepare("UPDATE wp_wc_product_meta_lookup SET min_price = ?, max_price = ? WHERE product_id = ?");
            $stmt->bind_param('ddi', $newPrice, $newPrice, $idProduct);
            $stmt->execute();

            $metaKeyPrice = '_price';
            $metaKeyRegularPrice = '_regular_price';
            $stmt = $this->db->prepare("UPDATE wp_postmeta SET meta_value = ? WHERE post_id = ? AND (meta_key = ? OR meta_key = ?)");
            $stmt->bind_param('diss', $newPrice, $idProduct, $metaKeyPrice, $metaKeyRegularPrice);
            $stmt->execute();
        }

        $stock = $product->availability->inStoreAvailability == "Available" || $product->availability->onlineAvailability == "InStock" ? "instock" : "outofsotck";

        $this->addGalleryImage($product->additionalMedia, $idProduct, $sku);

        if($stock != $result['stock_status']){
            $stmt = $this->db->prepare("UPDATE wp_wc_product_meta_lookup SET stock_status = ? WHERE product_id = ?");
            $stmt->bind_param('si', $stock, $idProduct);
            $stmt->execute();

            $stmt = $this->db->prepare("UPDATE wp_postmeta SET meta_value = ? WHERE meta_key = '_stock_status' 
                AND post_id = ?");
            $stmt->bind_param('si', $stock, $idProduct);
            $stmt->execute();

            $modify = true;
        }

        foreach($product->specs as $spec){
            $attributeCat = str_replace(' ', '-', strtolower($spec->name));
            $stmt = $this->db->prepare("SELECT * FROM wp_woocommerce_attribute_taxonomies WHERE attribute_name = ?");
            $stmt->bind_param('s', $attributeCat);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows == 0){
                $attributeType = 'select';
                $attributeOrder = 'menu_order';
                $attributePublic = 0;
                $insert = $this->db->prepare("INSERT INTO wp_woocommerce_attribute_taxonomies (attribute_name, attribute_label, attribute_type, attribute_orderby, attribute_public) 
                VALUES (?, ?, ?, ?, ?)");
                $insert->bind_param('ssssi', $attributeCat, $attributeCat, $attributeType, $attributeOrder, $attributePublic);
                $insert->execute();
                unset($insert);
            }

            unset($stmt);
            $attribute = $spec->value;
            $attributeCat = "pa_".$attributeCat;
            $stmt = $this->db->prepare("SELECT a.term_id FROM wp_terms a 
                JOIN wp_term_taxonomy b on (a.term_id = b.term_id)  WHERE a.name = ? AND b.taxonomy = ?");
            $stmt->bind_param('ss', $attribute, $attributeCat);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows == 0){
                $description = '';
                $maxId = $this->db->query("SELECT MAX(term_taxonomy_id) FROM wp_term_taxonomy");
                $maxId = (int)$maxId->fetch_row()[0] +1;
                $insert = $this->db->prepare("INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description) 
                VALUES (?, ?, ?, ?)");
                $insert->bind_param('iiss', $maxId, $maxId, $attributeCat, $description);
                $insert->execute();

                $slugAttribute = str_replace(' ', '-', $attribute);
                unset($insert);
                $insert = $this->db->prepare("INSERT INTO wp_terms (term_id, name, slug) VALUES (?, ?, ?)");
                $insert->bind_param('iss', $maxId, $attribute, $slugAttribute);
                $insert->execute();    
                unset($insert);
            }

            $stmt = $this->db->prepare("SELECT a.term_id FROM wp_terms a 
            JOIN wp_term_taxonomy b on (a.term_id = b.term_id)  WHERE a.name = ? AND b.taxonomy = ?");
            $stmt->bind_param('ss', $attribute, $attributeCat);
            $stmt->execute();
            $attribueId = $stmt->get_result()->fetch_assoc()['term_id'];

            unset($stmt);
            $stmt = $this->db->prepare("SELECT * FROM wp_term_relationships WHERE object_id = ? AND term_taxonomy_id = ?");
            $stmt->bind_param('ii', $idProduct, $attribueId);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows == 0){
                $insert = $this->db->prepare("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id) VALUES (?, ?)");
                $insert->bind_param('ii', $idProduct, $attribueId);
                $insert->execute();
                $modifySpec = true;
            }            
        }

        if($modifySpec){
            $like = 'pa_';
            $stmt = $this->db->prepare("SELECT distinct a.taxonomy FROM wp_term_taxonomy a 
                        JOIN wp_terms b on (a.term_id = b.term_id)
                        JOIN wp_term_relationships c on (b.term_id = c.term_taxonomy_id)
                        WHERE c.object_id = ? AND a.taxonomy LIKE CONCAT (?, '%')");
            $stmt->bind_param('is', $idProduct, $like);
            $stmt->execute();

            $attributeMeta = 's:%d:"%s";a:6:{s:4:"name";s:%d:"%s";s:5:"value";s:0:"";s:8:"position";s:%d:"%d";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}';
            $allAttibuteMeta = 'a:%d:{%s}';
            $attributesMeta = '';
            $result = $stmt->get_result();
            $numberAttributeType = 0;
            while($row = $result->fetch_assoc()){
                $attributesMeta .= sprintf($attributeMeta, strlen($row['taxonomy']), $row['taxonomy'], strlen($row['taxonomy']), $row['taxonomy'], strlen($numberAttributeType), $numberAttributeType);
                $numberAttributeType++;
            }
            $attributesMeta = sprintf($allAttibuteMeta, $numberAttributeType, $attributesMeta);

            unset($insert);
            $metaKey = '_product_attributes';

            $stmt = $this->db->prepare("SELECT * FROM wp_postmeta WHERE post_id = ? AND meta_key = ?");
            $stmt->bind_param('is', $idProduct, $metaKey);
            $stmt->execute();
            $result = $stmt->get_result();
            if($result->num_rows == 0)
                $insertOrUpdate = $this->db->prepare("INSERT INTO wp_postmeta (meta_value, post_id, meta_key) VALUES (?, ?, ?)");
            else
                $insertOrUpdate = $this->db->prepare("UPDATE wp_postmeta SET meta_value = ? WHERE post_id = ? AND meta_key = ?");

            $insertOrUpdate->bind_param('sis', $attributesMeta, $idProduct, $metaKey);
            $insertOrUpdate->execute();
        }

        $stmt = $this->db->prepare("SELECT * FROM wp_posts WHERE ID = ? AND post_content LIKE CONCAT('%',?)");
        $stmt->bind_param('is', $idProduct, $product->longDescription);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0){
            $postContent = $product->shortDescription." ".$product->longDescription;
            $update = $this->db->prepare("UPDATE wp_posts SET post_content = ? WHERE ID = ?");
            $update->bind_param('si', $postContent, $idProduct);
            $update->execute();
            $modifySpec = true;
        }

        if($modifyStock || $modifySpec || $modifyPrice){
            header("Refresh:0");
            exit();
        }
    }


    public function addGalleryImage($images, $idProduct, $sku){
        $gallery = [];
        $attachedMetaId = $this->getIdPlaceholder($idProduct);
        if($attachedMetaId != false){
            $this->db->query("UPDATE wp_postmeta SET meta_value = '".$images[0]->url."' WHERE meta_id = ".$attachedMetaId);
        }
        array_shift($images);
        foreach($images as $image){
            $stmt = $this->db->prepare("SELECT ID FROM wp_posts WHERE GUID = ?");
            $stmt->bind_param('s', $image->url);
            $stmt->execute();
            $result = $stmt->get_result();

            if($result->num_rows == 0){
                $insertPost = $this->db->prepare("INSERT INTO wp_posts (post_author, post_date, post_date_gmt, post_title, post_status, comment_status, ping_status, post_name, post_modified, post_modified_gmt, guid, post_type, post_mime_type, post_excerpt, to_ping, pinged, post_content_filtered, post_content) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $date = date('Y-m-d H:i:s');
                $dateGmt = gmdate('Y-m-d H:i:s');
                $author = 1;
                $type = 'attachment';
                $postStatus = 'inherit';
                $commentStatus = 'open';
                $pingStatus = 'closed';
                $mime = 'image/jpeg';
                $nullString = '';
                $insertPost->bind_param('ississsissssssssss', $author, $date, $dateGmt, $sku, $postStatus, $commentStatus,
                            $pingStatus, $sku, $date, $dateGmt, $image->url, $type, $mime, $nullString, $nullString, $nullString, $nullString, $nullString);
                $insertPost->execute();
                $id = $insertPost->insert_id;

                $insertPostMeta = $this->db->prepare("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES (?, ?, ?)");
                
                $metaKey = '_wp_attached_file';
                $insertPostMeta->bind_param('iss', $id, $metaKey, $image->url);
                $insertPostMeta->execute();

                $metaKey = '_wp_attachment_metadata';
                $metaValue = str_replace('SKUIMAGETOREPLACE', $sku, $this->postMeta2[$metaKey]);
                $insertPostMeta->bind_param('iss', $id, $metaKey, $metaValue);
                $insertPostMeta->execute();

                $gallery[] = $id;
            }else{
                $gallery[] = $result->fetch_assoc()['ID'];
            }
        }

        $update = $this->db->prepare("UPDATE wp_postmeta SET meta_value = ? WHERE post_id = ? AND meta_key = ?");
        $galleryString = implode(',', $gallery);
        $metaKey = '_product_image_gallery';
        $update->bind_param('sis', $galleryString, $idProduct, $metaKey);
        $update->execute();
    }

    public function getProduct(){
        return $this->products;
    }

    private function getIdPlaceholder($productId){
        $query = $this->db->query("SELECT meta_value FROM wp_postmeta WHERE post_id = ".$productId." AND meta_key = '_thumbnail_id'");
        $attachedId = $query->fetch_assoc()['meta_value'];
        $query = $this->db->query("SELECT meta_value, meta_id FROM wp_postmeta WHERE post_id = ".$attachedId." AND meta_key = '_wp_attached_file'");
        $res = $query->fetch_assoc();
        $urlImage = $res['meta_value'];
        $attachedMetaId = $res['meta_id'];
        if($urlImage == 'https://marinedutyfree.com/wp-content/uploads/2019/11/placeholder.jpg'){
            return $attachedMetaId;
        }

        return false;
    }

    /**
     * Ajoute la marge de Marin duty free 
     * Puis on change le taux CAD => USD
     * On arrondit au dessus à 49 ou 99 cents
     */
    private function convertCADToUSD($cad, $substract = 0){
        $taxMarine = $ubstract > 0  && $ubstract <= 100 ? self::TAXE_MARINE_DUTY_FREE - self::TAXE_MARINE_DUTY_FREE * $substract/100 : self::TAXE_MARINE_DUTY_FREE;
        $cad += $cad * $taxMarine/100;
        $cad = ceil($cad*$this->CADToUSD*pow(10,2))/pow(10,2);
        $priceArray = explode('.', $cad);
        if($priceArray[1] <= 49)
            $cad = $priceArray[0].".49";
        else
            $cad = $priceArray[0].".99";

        return (float)$cad;
    }

    private function getSubstractPercentage($idProduct){
        $metaKey = "substract_percentage";
        $stmt = $this->db->prepare("SELECT meta_value FROM wp_postmeta WHERE post_id = ? AND meta_key = ?");
        $stmt->bind_param('is', $idProduct, $metaKey);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->num_rows == 1 ? $result->fetch_assoc()['meta_value'] : 0;
    }

    private function getActualPrice($idProduct){
        $metaKey = "substract_percentage";
        $stmt = $this->db->prepare("SELECT min_price FROM wp_wc_product_meta_lookup WHERE product_id = ?");
        $stmt->bind_param('i', $idProduct);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->num_rows == 1 ? $result->fetch_assoc()['min_price'] : 0;
    }
}